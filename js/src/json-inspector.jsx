/* eslint-env browser */
/* global window */
'use strict';

const React = require('react');
const ReactDOM = require('react-dom');

const Inspector = require('react-json-inspector');
require('react-json-inspector/json-inspector.css');
require('./json-inspector.css');

ReactDOM.render(
  <Inspector data={window.jsonInspectorData} />,
  document.getElementById('json-inspector')
);

---
layout: md_page
title: Documentation
---

# OGP Toolbox

During the Open Government Partnership Summit in Paris on December 7th, 8th and 9th 2016, there will be a *hackathon* on *civic techs*. The goal is to present a a first version of a toolbox for open government.

The OGP toolbox is based on the [Open Software Base](https://framagit.org/codegouv/open-software-base-yaml).

The toolbox takes data from the [Open Software Base](https://framagit.org/codegouv/open-software-base-yaml), filtering programs based on their sources.

Two sources are currently used in the toolbox:

[Civicstack](http://www.civicstack.org/)

* Script: https://git.framasoft.org/codegouv/civicstack-to-yaml
* Data: https://git.framasoft.org/codegouv/civicstack-yaml
* Typical YAML sheet for a program : https://git.framasoft.org/codegouv/civicstack-yaml/blob/master/d/democracyos.yaml

[Tech Platforms for Civic-Participation](https://docs.google.com/spreadsheets/d/1YBZLdNsGohGBjO5e7yrwOQx78IzCA6SNW6T14p15aKU)

* Script: https://framagit.org/codegouv/tech-plateforms-to-yaml
* Data: https://framagit.org/codegouv/tech-plateforms-yaml

module.exports = {
  entry: {
    'json-inspector': './js/src/json-inspector.jsx'
  },
  output: {
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /json-inspector.jsx$/,
        loader: 'babel-loader?presets[]=react,presets[]=es2015'
      },
      {test: /\.css$/, loader: 'style-loader!css-loader'}
    ]
  }
};

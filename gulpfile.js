const concat = require('gulp-concat');
const gulp = require('gulp');
const uglify = require('gulp-uglify');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require("./webpack.config.js");

gulp.task('javascripts', function() {
  var myConfig = Object.create(webpackConfig);
  myConfig.plugins = myConfig.plugins || [];
  myConfig.plugins = myConfig.plugins.concat(
    new webpack.DefinePlugin({
      'process.env': {NODE_ENV: JSON.stringify("production")}
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin()
  );

  gulp.src('js/src/*.jsx')
    .pipe(webpackStream(myConfig))
    .pipe(uglify())
    .pipe(gulp.dest('js/dist'))
  ;

  gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    'node_modules/selectize/dist/js/standalone/selectize.min.js',
    'node_modules/masonry-layout/dist/masonry.pkgd.min.js'
  ])
    .pipe(concat('exports.js'))
    .pipe(gulp.dest('js/dist'))
  ;
});

gulp.task('default', ['javascripts']);

gulp.task('watch', ['default'], function() {
  gulp.watch(['/js/src/*.js*', '/js/src/*.jsx'], ['javascripts']);
});

module Jekyll
  class ProgramPage < Page
    def initialize(site, base, source_file)
      @site = site
      @base = base
      @dir = "program"

      begin
        self.content = File.read(source_file.path,
                                 Utils.merged_file_read_opts(site, {}))
        self.data= {}
        self.data["program"] = SafeYAML.load(self.content)
        self.content = ""
      rescue SyntaxError => e
        Jekyll.logger.warn "YAML Exception reading #{source_file.path}: #{e.message}"
      rescue Exception => e
        Jekyll.logger.warn "Error reading file #{source_file.path}: #{e.message}"
      end

      @name = self.data["program"]["name"] + ".html"
      @path = site.in_source_dir(@base, @dir, @name)
      process(@name)

      filename = File.join(base, name)
      validate_data! filename
      validate_permalink! filename

      self.data["title"] = self.data["program"]["name"]
      self.data["layout"] = "program"
    end
  end

  class OpenSoftwareBaseGenerator < Generator
    def generate(site)
      site.data["programs"] = {}
      files = site.static_files.select do |file|
        /node_modules\/etalab-open-software-base-yaml\/(.*)\.yaml$/ === (file.path)
      end

      files.each do |source_file|
        content = File.read(source_file.path, Utils.merged_file_read_opts(site, {}))
        data = SafeYAML.load(content)
        unless data["civicstack"].nil? && data["tech-plateforms"].nil? && data["nuit-debout"].nil? && data["participatedb"].nil? && data["ogptoolbox-framacalc"].nil?
          site.pages << ProgramPage.new(site, site.source, source_file)
          site.data["programs"][data["name"]] = data
        end
      end

      site.data["programsList"] = site.data["programs"].values

      site.data["statistics"] = {}
      site.data["programsList"].each do |program|

        sources = program.select do |k,v|
          v.class.to_s == "Hash" && v.has_key?("_source")
        end

        sources.each do |source_id, source|
          if site.data["statistics"][source_id].nil?
            site.data["statistics"][source_id] = {}
            site.data["statistics"][source_id]["fields"] = {}
            site.data["statistics"][source_id]["total"] = 0
            site.data["statistics"][source_id]["_source"] = source["_source"]
          end
          site.data["statistics"][source_id]["total"] += 1

          fields = source.keep_if{ |k,v| k != '_source' }

          fields.each do |field_name, field|
            if site.data["statistics"][source_id]["fields"][field_name].nil?
              site.data["statistics"][source_id]["fields"][field_name] = 0
            end

            if field
              site.data["statistics"][source_id]["fields"][field_name] += 1
            end
          end # end list count fields

        end # end programs loop

      end
    end
  end # end Class
end
